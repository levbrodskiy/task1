package ru.blm.spr;

import java.util.Collection;

public class SearchText implements Runnable{
    private String path;
    private String sText;
    private Collection<String> containsText;

    public SearchText(String path, String sText, Collection<String> containsText) {
        this.path = path;
        this.sText = sText;
        this.containsText = containsText;
    }

    @Override
    public void run() {
        if (new ReadFile(path).read().contains(sText)){
            containsText.add(path);
        }
    }
}
