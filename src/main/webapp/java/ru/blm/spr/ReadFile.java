package ru.blm.spr;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

public class ReadFile {
    private String path;

    public ReadFile(String fileName) {
        this.path = fileName;
    }

    public String read() {
        StringBuilder data = new StringBuilder("");
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(
                new FileInputStream(path), Charset.forName("UTF-8")))){
            String line;
            while ((line = reader.readLine()) != null) {
                data.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new String(data);
    }
}
