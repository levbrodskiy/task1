package ru.blm.spr;

import java.io.File;
import java.util.*;


public class FileSystem {
    private String rootPath;

    public FileSystem(String rootPath) {
        this.rootPath = rootPath;
    }

    public List<String> getTxtFiles(){
        List<String> result = new ArrayList<>();
        Queue<File> allFiles = new PriorityQueue<>();

        File rootDir = new File(rootPath);

        Collections.addAll(allFiles,rootDir.listFiles());

        while (!allFiles.isEmpty()){
            File currentFile = allFiles.remove();

            if(currentFile.isDirectory()){
                Collections.addAll(allFiles,currentFile.listFiles());

            }else if (currentFile.getName().endsWith(".txt")){
                result.add(currentFile.getAbsolutePath());
            }

        }
        return result;
    }

}
